<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="assets/css/main.min.css">
</head>
<body>

	<div id="page-top"></div>
    
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="assets/js/custom-dist.js"></script> 

		<div id="top" class="navbar-fixed-top">
			<div id="top-bar">
				<div class="container">
					<div class="top-bar__info hidden-xs">
						<p>CALL US ON: <a href="tel:+973 123 456 7890">+973 123 456 7890</a></p>
					</div>
					<!-- // info  -->
					<div class="top-bar__links">
						<ul>
							<li><a href="#"><i class="fas fa-user"></i>MY ACCOUNT</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">CONTACT US</a></li>
						</ul>
					</div>
					<!-- // links  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // top bar  -->
			<div id="menu-bar">
				<div class="container">
					<div class="branding">
						<a href="index.php"><img src="assets/img/logos/website-logo.svg" alt=""></a>
					</div>
					<!-- // branding  -->
					<div id="main-menu">
						<?php include('inc/nav.php'); ?>
					</div>
					<!-- // main menu  -->
					<div id="actions-bar">
						<ul>
							<li><a href="#"><i class="far fa-search"></i></a></li>
						</ul>
					</div>
					<!-- // actions  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // menu bar  -->
		</div>
        <!-- // top bar  -->
        
        <div id="breadcrumb">
            <div class="container">
                Breadcrumb
            </div>
        </div>
        <!-- // breadcrumb  -->

        <div id="artistian-listing">
            <div class="container">
                <nav>
                    <ul>
                        <li class="active"><a href="#">INTERIOR DESIGN</a></li>
                        <li><a href="#">ARCHITECTURE</a></li>
                        <li><a href="#">CONTRACTOR</a></li>
                    </ul>
                </nav>
                <!-- // nav  -->
				<div id="filter-bar">
				</div>
				<!-- // filter bar  -->
				<div id="artisians">
					<div class="row">

						<div class="col-md-6 col-sm-12">
							<div class="pro-block">
								<a href="#">
									<img src="assets/img/bg/art.jpg" alt="" class="img-responsive">
									<div class="overlay"></div>
									<div class="caption">
										<h4>Casa Lusso Furniture</h4>
										<div class="stars">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
										</div>
										<!-- // stars  -->
									</div>
									<!-- // caption  -->
									<span href="#" class="btn-more">Explore more</span>
								</a>
								<!-- // link  -->
							</div>
						</div>
						<!-- // block  -->

						<div class="col-md-6 col-sm-12">
							<div class="pro-block">
								<a href="#">
									<img src="assets/img/bg/art.jpg" alt="" class="img-responsive">
									<div class="overlay"></div>
									<div class="caption">
										<h4>Casa Lusso Furniture</h4>
										<div class="stars">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
										</div>
										<!-- // stars  -->
									</div>
									<!-- // caption  -->
									<span href="#" class="btn-more">Explore more</span>
								</a>
								<!-- // link  -->
							</div>
						</div>
						<!-- // block  -->
						
						<div class="col-md-6 col-sm-12">
							<div class="pro-block">
								<a href="#">
									<img src="assets/img/bg/art.jpg" alt="" class="img-responsive">
									<div class="overlay"></div>
									<div class="caption">
										<h4>Casa Lusso Furniture</h4>
										<div class="stars">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
										</div>
										<!-- // stars  -->
									</div>
									<!-- // caption  -->
									<span href="#" class="btn-more">Explore more</span>
								</a>
								<!-- // link  -->
							</div>
						</div>
						<!-- // block  -->
						
						<div class="col-md-6 col-sm-12">
							<div class="pro-block">
								<a href="#">
									<img src="assets/img/bg/art.jpg" alt="" class="img-responsive">
									<div class="overlay"></div>
									<div class="caption">
										<h4>Casa Lusso Furniture</h4>
										<div class="stars">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
										</div>
										<!-- // stars  -->
									</div>
									<!-- // caption  -->
									<span href="#" class="btn-more">Explore more</span>
								</a>
								<!-- // link  -->
							</div>
						</div>
						<!-- // block  -->
						
						<div class="col-md-6 col-sm-12">
							<div class="pro-block">
								<a href="#">
									<img src="assets/img/bg/art.jpg" alt="" class="img-responsive">
									<div class="overlay"></div>
									<div class="caption">
										<h4>Casa Lusso Furniture</h4>
										<div class="stars">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
										</div>
										<!-- // stars  -->
									</div>
									<!-- // caption  -->
									<span href="#" class="btn-more">Explore more</span>
								</a>
								<!-- // link  -->
							</div>
						</div>
						<!-- // block  -->
						
						<div class="col-md-6 col-sm-12">
							<div class="pro-block">
								<a href="#">
									<img src="assets/img/bg/art.jpg" alt="" class="img-responsive">
									<div class="overlay"></div>
									<div class="caption">
										<h4>Casa Lusso Furniture</h4>
										<div class="stars">
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
											<i class="fas fa-star"></i>
										</div>
										<!-- // stars  -->
									</div>
									<!-- // caption  -->
									<span href="#" class="btn-more">Explore more</span>
								</a>
								<!-- // link  -->
							</div>
						</div>
						<!-- // block  -->						

					</div>
					<!-- // row  -->
				</div>
				<!-- // listing  -->
            </div>
        </div>
        <!-- // artisian listing  -->

		<?php include_once('inc/footer.php'); ?>

</body>
</html>
