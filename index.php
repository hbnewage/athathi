<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="assets/css/main.min.css">
</head>
<body>

	<div id="page-top"></div>
    
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="assets/js/custom-dist.js"></script> 

		<div id="top" class="navbar-fixed-top">
			<div id="top-bar">
				<div class="container">
					<div class="top-bar__info hidden-xs">
						<p>CALL US ON: <a href="tel:+973 123 456 7890">+973 123 456 7890</a></p>
					</div>
					<!-- // info  -->
					<div class="top-bar__links">
						<ul>
							<li><a href="#"><i class="fas fa-user"></i>MY ACCOUNT</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">CONTACT US</a></li>
						</ul>
					</div>
					<!-- // links  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // top bar  -->
			<div id="menu-bar">
				<div class="container">
					<div class="branding">
						<a href="index.php"><img src="assets/img/logos/website-logo.svg" alt=""></a>
					</div>
					<!-- // branding  -->
					<div id="main-menu">
						<?php include('inc/nav.php'); ?>
					</div>
					<!-- // main menu  -->
					<div id="actions-bar">
						<ul>
							<li><a href="#"><i class="far fa-search"></i></a></li>
						</ul>
					</div>
					<!-- // actions  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // menu bar  -->
		</div>
		<!-- // top bar  -->

		<header id="hero-banner">
			<div id="hero-slider">

				<div class="item hero-slide">
					<div class="overlay"></div>
					<img src="assets/img/bg/slide-1.jpg" alt="">
					<div class="caption">
						<div class="container">
							<div class="holder">
								<h6 data-animation="fadeInDown" data-delay="0.3s">BAHRAIN FURNITURE</h6>
								<h2 data-animation="fadeInUp" data-delay="0.4s">Welcome to the World of Luxury Furniture</h2>
								<a data-animation="fadeInUp" data-delay="0.6s" href="#" class="btn-cta">EXPLORE NOW</a>
							</div>
							<!-- // holder  -->
						</div>
					</div>
					<!-- // caption  -->
				</div>
				<!-- // item  -->

				<div class="item hero-slide">
					<div class="overlay"></div>
					<img src="assets/img/bg/slide-1.jpg" alt="">
					<div class="caption">
						<div class="container">
							<div class="holder">
								<h6 data-animation="fadeInDown" data-delay="0.3s">BAHRAIN FURNITURE</h6>
								<h2 data-animation="fadeInUp" data-delay="0.4s">Welcome to the World of Luxury Furniture</h2>
								<a data-animation="fadeInUp" data-delay="0.6s" href="#" class="btn-cta">EXPLORE NOW</a>
							</div>
							<!-- // holder  -->
						</div>
					</div>
					<!-- // caption  -->
				</div>
				<!-- // item  -->

				<div class="item hero-slide">
					<div class="overlay"></div>
					<img src="assets/img/bg/slide-1.jpg" alt="">
					<div class="caption">
						<div class="container">
							<div class="holder">
								<h6 data-animation="fadeInDown" data-delay="0.3s">BAHRAIN FURNITURE</h6>
								<h2 data-animation="fadeInUp" data-delay="0.4s">Welcome to the World of Luxury Furniture</h2>
								<a data-animation="fadeInUp" data-delay="0.6s" href="#" class="btn-cta">EXPLORE NOW</a>
							</div>
							<!-- // holder  -->
						</div>
					</div>
					<!-- // caption  -->
				</div>
				<!-- // item  -->				
				

			</div>
			<!-- // hero slider  -->

			<div class="category-wrapper">
				<div class="container">

					<div id="category-slider">

						<div class="item">
							<a href="#">
								<div class="icon-holder">
									<img src="assets/img/ico/service-1.png" alt="">
								</div>
								<div class="content">
									<small>BED</small>
								</div>
							</a>
						</div>
						<!-- // item  -->

						<div class="item">
							<a href="#">
								<div class="icon-holder">
									<img src="assets/img/ico/service-2.png" alt="">
								</div>
								<div class="content">
									<small>TABLES</small>
								</div>
							</a>
						</div>
						<!-- // item  -->
						
						<div class="item">
							<a href="#">
								<div class="icon-holder">
									<img src="assets/img/ico/service-3.png" alt="">
								</div>
								<div class="content">
									<small>SEATING</small>
								</div>
							</a>
						</div>
						<!-- // item  -->
						
						<div class="item">
							<a href="#">
								<div class="icon-holder">
									<img src="assets/img/ico/service-4.png" alt="">
								</div>
								<div class="content">
									<small>STORAGE</small>
								</div>
							</a>
						</div>
						<!-- // item  -->
						
						<div class="item">
							<a href="#">
								<div class="icon-holder">
									<img src="assets/img/ico/service-5.png" alt="">
								</div>
								<div class="content">
									<small>LIGHTING</small>
								</div>
							</a>
						</div>
						<!-- // item  -->
						
						<div class="item">
							<a href="#">
								<div class="icon-holder">
									<img src="assets/img/ico/service-6.png" alt="">
								</div>
								<div class="content">
									<small>FLOORING</small>
								</div>
							</a>
						</div>
						<!-- // item  -->
						
						<div class="item">
							<a href="#">
								<div class="icon-holder">
									<img src="assets/img/ico/service-1.png" alt="">
								</div>
								<div class="content">
									<small>BED</small>
								</div>
							</a>
						</div>
						<!-- // item  -->						

					</div>
					<!-- // slider  -->

				</div>
			</div>
			<!-- // wrapper  -->

		</header>
		<!-- // hero banner  -->

		<section class="product-list">
			<div class="container">
				<header>
					<h3>Sale</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscin</p>
				</header>

				<div class="products-slider row">

					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->

					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Fuchsia Tufted Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // item  -->			
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->

					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Fuchsia Tufted Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // item  -->									
					

				</div>
				<!-- // products  -->

				<footer>
					<a href="#" class="btn-cta"><span>VIEW ALL</span></a>
				</footer>

			</div>
			<!-- // container  -->
		</section>
		<!-- // product sale  -->

		<section id="middle-cta">
			<div class="overlay">
				<div class="container">
					<div class="holder">
						<h4>Exclusive First Look</h4>
						<h1>#HandPicked By Our Beauty Experts</h1>
						<a href="#" class="btn-more"> READ MORE</a>
					</div>
				</div>
			</div>
		</section>
		<!-- // middle cta  -->

		<section class="product-list">
			<div class="container">
				<header>
					<h3>New Arrivals</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscin</p>
				</header>

				<div class="products-slider row">

					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->

					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Fuchsia Tufted Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // item  -->			
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->

					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Fuchsia Tufted Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // item  -->									
					

				</div>
				<!-- // products  -->

				<footer>
					<a href="#" class="btn-cta"><span>VIEW ALL</span></a>
				</footer>

			</div>
			<!-- // container  -->
		</section>
		<!-- // product sale  -->		

		<section id="artist-list">
			<div class="container">
				<div class="row">

					<div class="col-sm-6">
						<div class="artist-block">
							<a href="#">
								<img src="assets/img/misc/artist1.jpg" alt="" class="img-responsive">
								<div class="overlay"></div>
								<div class="caption">
									<h4>Be Concept</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscin lorem solo tempor</p>
									<span  class="btn-more">Continue Reading</span>
								</div>
								<!-- // caption  -->
							</a>
						</div>
					</div>
					<!-- // artist block col  -->


					<div class="col-sm-6">
						<div class="artist-block">
							<a href="#">
								<img src="assets/img/misc/artist2.jpg" alt="" class="img-responsive">
								<div class="overlay"></div>
								<div class="caption">
									<h4>Marina House</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscin lorem solo tempor</p>
									<span  class="btn-more">Continue Reading</span>
								</div>
								<!-- // caption  -->
							</a>
						</div>
					</div>
					<!-- // artist block col  -->
					

					<div class="col-sm-6">
						<div class="artist-block">
							<a href="#">
								<img src="assets/img/misc/artist2.jpg" alt="" class="img-responsive">
								<div class="overlay"></div>
								<div class="caption">
									<h4>Be Concept</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscin lorem solo tempor</p>
									<span  class="btn-more">Continue Reading</span>
								</div>
								<!-- // caption  -->
							</a>
						</div>
					</div>
					<!-- // artist block col  -->
					

					<div class="col-sm-6">
						<div class="artist-block">
							<a href="#">
								<img src="assets/img/misc/artist1.jpg" alt="" class="img-responsive">
								<div class="overlay"></div>
								<div class="caption">
									<h4>Be Concept</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscin lorem solo tempor</p>
									<span  class="btn-more">Continue Reading</span>
								</div>
								<!-- // caption  -->
							</a>
						</div>
					</div>
					<!-- // artist block col  -->					

				</div>
				<!-- // row  -->
			</div>
			<!-- // container  -->
		</section>
		<!-- // artist  -->

		<?php include_once('inc/instagram.php'); ?>

		<?php include_once('inc/footer.php'); ?>

</body>
</html>
