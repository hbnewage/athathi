//@prepros-prepend libs/bootstrap.min.js
//@prepros-prepend libs/easing.js
//@prepros-prepend libs/jquery-ui.min.js
//@prepros-prepend libs/slick.min.js
//@prepros-prepend libs/jquery.matchHeight.js
//@prepros-prepend libs/wow.min.js


$(document).ready(function(){

    new WOW().init();
    
        $('#hero-slider').on('init', function(e, slick) {
            var $firstAnimatingElements = $('div.hero-slide:first-child').find('[data-animation]');
            doAnimations($firstAnimatingElements);    
        });
        $('#hero-slider').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
                  var $animatingElements = $('div.hero-slide[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
                  doAnimations($animatingElements);    
        });
        $('#hero-slider').slick({
           autoplay: false,
           autoplaySpeed: 10000,
           arrows:false,
           dots: true,
           fade: true,
            autoplay: true,
            autoplaySpeed: 6500,
            pauseOnFocus: false,
            pauseOnHover:false,
            pauseOnDotsHover: false,     
        });
        function doAnimations(elements) {
            var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            elements.each(function() {
                var $this = $(this);
                var $animationDelay = $this.data('delay');
                var $animationType = 'animated ' + $this.data('animation');
                $this.css({
                    'animation-delay': $animationDelay,
                    '-webkit-animation-delay': $animationDelay
                });
                $this.addClass($animationType).one(animationEndEvents, function() {
                    $this.removeClass($animationType);
                });
            });
        }
        
        // Sticky header
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50){  
                $('#top-bar').addClass("sticky");
            }
            else{
                $('#top-bar').removeClass("sticky");
            }
        });

        // Sticky header
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50){  
                $('#to-top').addClass("active-top");
            }
            else{
                $('#to-top').removeClass("active-top");
            }
        });        

        $("#category-slider").slick({
            // normal options...
            infinite: true,
            dots: false,
            arrows:true,
            slidesToShow: 6,
            slidesToScroll: 6,
            autoplay: false,
            autoplaySpeed: 5500,
            pauseOnFocus: false,
            pauseOnHover:false,
            pauseOnDotsHover: false,    
            responsive: [
                {
                  breakpoint: 580,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    adaptiveHeight: true
                  }
                },
                {
                  breakpoint: 1000,
                  settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    adaptiveHeight: true
                  }
                },                
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
              ]                     
          });

          $("#products-slider").slick({
            // normal options...
            infinite: true,
            dots: false,
            arrows:true,
            slidesToShow: 3,
            slidesToScroll: 3,
            autoplay: false,
            autoplaySpeed: 5500,
            pauseOnFocus: false,
            pauseOnHover:false,
            pauseOnDotsHover: false,    
            responsive: [
                {
                  breakpoint: 680,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: true,
                    arrows:false,
                  }
                },
                {
                  breakpoint: 1000,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,                
                  }
                },                
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
              ]                     
          });          
       
          $("#images-slider").slick({
            // normal options...
            infinite: true,
            dots: true,
            arrows:false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5500,
            pauseOnFocus: false,
            pauseOnHover:false,
            pauseOnDotsHover: false,                        
          });
       

          
        // Menu
        $('#mobile-menu--btn a').click(function(){
            $('.main-menu-sidebar').addClass("menu-active");
            $('.menu-overlay').addClass("active-overlay");
            $('body').toggleClass('body-scroll');
            $('html').toggleClass('body-scroll');
            $(this).toggleClass('open');
        });

        // Menu
        $('.close-menu-btn').click(function(){
            $('.main-menu-sidebar').removeClass("menu-active");
            $('.menu-overlay').removeClass("active-overlay");
            $('body').toggleClass('body-scroll');
            $('html').toggleClass('body-scroll');
        });

            $(function() {
        
            var menu_ul = $('.nav-links > li.has-menu > ul'),
                menu_a  = $('.nav-links > li.has-menu > a');
            
            menu_ul.hide();
            
            menu_a.click(function(e) {
                e.preventDefault();
                if(!$(this).hasClass('active')) {
                menu_a.removeClass('active');
                menu_ul.filter(':visible').slideUp('normal');
                $(this).addClass('active').next().stop(true,true).slideDown('normal');
                } else {
                $(this).removeClass('active');
                $(this).next().stop(true,true).slideUp('normal');
                }
            });
            
            });
            
        $(".nav-links > li.has-menu > a ").attr("href","javascript:;");

        $(function() {
        $('.product-sale .products-slider .product-block .content').matchHeight();
        $('.product-sale .products-slider .product-block').matchHeight();
        $('.product-list .products-slider .product-block .content h5').matchHeight();
        });     
        
        $(document).ready( function() {
            $('#accordion').accordion({
                collapsible:true,
                  heightStyle: "content" ,
                beforeActivate: function(event, ui) {
                     // The accordion believes a panel is being opened
                    if (ui.newHeader[0]) {
                        var currHeader  = ui.newHeader;
                        var currContent = currHeader.next('.ui-accordion-content');
                     // The accordion believes a panel is being closed
                    } else {
                        var currHeader  = ui.oldHeader;
                        var currContent = currHeader.next('.ui-accordion-content');
                    }
                     // Since we've changed the default behavior, this detects the actual status
                    var isPanelSelected = currHeader.attr('aria-selected') == 'true';
                    
                     // Toggle the panel's header
                    currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('ui-accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));
                    
                    // Toggle the panel's icon
                    currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);
                    
                     // Toggle the panel's content
                    currContent.toggleClass('accordion-content-active',!isPanelSelected)    
                    if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }
        
                    return false; // Cancels the default action
                }
            });
        });
                
        $(function() {
            $('a[href*="#page-top"]:not([href="#"])').click(function() {
              if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                  $('html, body').animate({
                    scrollTop: target.offset().top
                  }, 1000);
                  return false;
                }
              }
            });
          });

          $(function() {
            $('a[href*="#rooms-types"]:not([href="#"])').click(function() {
              if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                  $('html, body').animate({
                    scrollTop: target.offset().top
                  }, 1000);
                  return false;
                }
              }
            });
          });          
          
        
    });
