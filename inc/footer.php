<footer id="page-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="footer-info">
                    <a href="index.php"><img src="assets/img/logos/website-logo.svg" alt=""></a>
                    <ul class="socials">
                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- // footer info col  -->
        </div>
        <!-- // row  -->
        <div class="row footer-links">

            <div class="col-md-3 col-sm-3">
                <div class="footer-sitemap">
                    <span class="title">SITEMAP</span>
                    <ul>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Delivery Information</a></li>
                    </ul>
                </div>
            </div>
            <!-- // sitemap 0 -->

            <div class="col-md-3 col-sm-3">
                <div class="footer-sitemap">
                    <span class="title">INFORMATION</span>
                    <ul>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Delivery Information</a></li>
                    </ul>
                </div>
            </div>
            <!-- // sitemap 0 -->
            
            <div class="col-md-3 col-sm-3">
                <div class="footer-sitemap">
                    <span class="title">ACCOUNT</span>
                    <ul>
                        <li><a href="#">My Account</a></li>
                        <li><a href="#">Register</a></li>
                        <li><a href="#">Forgotten password</a></li>
                        <li><a href="#">Newsletter</a></li>
                        <li><a href="#">Wishlist</a></li>
                    </ul>
                </div>
            </div>
            <!-- // sitemap 0 -->	
            
            <div class="col-md-3 col-sm-3">
                <div class="footer-contact">
                    <span class="title">ABOUT US</span>
                    <div class="content">
                        <p><i class="fas fa-envelope"></i> Email: <a href="mailto:info@athlathi.com">info@athlathi.com</a></p>
                        <p><i class="fas fa-phone"></i> Phone: <a href="tel:(+973) 1234 7890">(+973) 1234 7890</a></p>
                    </div>
                    <div class="cards">
                    <div class="card-block">
                            <i class="fab fa-cc-visa"></i>
                        </div>
                        <!-- // card block  -->
                        <div class="card-block">
                            <i class="fab fa-cc-mastercard"></i>
                        </div>
                        <!-- // card block  -->                                                
                    <div class="card-block">
                            <i class="fab fa-cc-paypal"></i>
                        </div>
                        <!-- // card block  -->                        
                        <div class="card-block">
                            <i class="fab fa-cc-amex"></i>
                        </div>
                        <!-- // card block  -->
                    </div>  
                    <!-- // cards  -->
                </div>
            </div>
            <!-- // contact  -->

        </div>
    </div>
</footer>
<!-- // page footer  -->

<div id="copy-bar">
    <div class="container">
        <a href="#page-top" class="to-top"><i class="fal fa-angle-up"></i></a>
        <div class="copy-notice">
            <small>Copyright  - All Right Reserved 2000 - 2019</small>
        </div>
        <!-- // notice  -->
        <div class="powered-by">
            <p>Powered by</p>
        </div>
        <!-- // powered  -->
    </div>
    <!-- // container  -->
</div>
<!-- // copy bar  -->