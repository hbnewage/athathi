<div class="navbar navbar-default yamm hidden-xs hidden-sm">
          <div id="navbar-collapse-grid" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <!-- Grid 12 Menu -->
              <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">FURNITURE<b class="caret-down"></b></a>
                <ul class="dropdown-menu">
                  <li class="grid">
                    <div class="row">

                      <div class="col-sm-3">
                          <div class="menu-block">
                            <span class="title">BED</span>
                            <ul>
                                <li><a href="#">Single Beds</a></li>
                                <li><a href="#">Double Beds</a></li>
                                <li><a href="#">King Size Beds</a></li>
                                <li><a href="#">Sofa Beds</a></li>
                                <li><a href="#">Bunk Beds</a></li>
                            </ul>
                          </div>
                      </div>
                      <!-- // menu block col  -->

                      <div class="col-sm-3">
                          <div class="menu-block">
                            <span class="title">TABLES</span>
                            <ul>
                                <li><a href="#">Dining</a></li>
                                <li><a href="#">Side Table</a></li>
                                <li><a href="#">Desk Table</a></li>
                                <li><a href="#">TV Units</a></li>
                                <li><a href="#">Outdoors</a></li>
                            </ul>
                          </div>
                      </div>
                      <!-- // menu block col  -->
                      
                      <div class="col-sm-3">
                          <div class="menu-block">
                            <span class="title">SEATING</span>
                            <ul>
                                <li><a href="#">Chair</a></li>
                                <li><a href="#">Sofa</a></li>
                                <li><a href="#">Stool</a></li>
                                <li><a href="#">Bean Bags</a></li>
                                <li><a href="#">Benches</a></li>
                            </ul>
                          </div>
                      </div>
                      <!-- // menu block col  -->
                      
                      <div class="col-sm-3">
                          <div class="menu-block">
                            <span class="title">STORAGE</span>
                            <ul>
                                <li><a href="#">Cabinets</a></li>
                                <li><a href="#">Wardrobe</a></li>
                                <li><a href="#">Bin & Baskets</a></li>
                                <li><a href="#">Bookcase & Shelves</a></li>
                                <li><a href="#">Racks & Rails</a></li>
                            </ul>
                          </div>
                      </div>
                      <!-- // menu block col  -->                      

                    </div>
                    <!-- // row  -->
                  </li>
                </ul>
              </li>
              <!--With Offsets 
              -->
              <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">LIGHTING<b class="caret-down"></b></a>
                <ul class="dropdown-menu">
                  <li class="grid">
                    <div class="row">
                      
                    </div>
                    <!-- // row  -->
                  </li>
                </ul>
              </li>
              <!--Aside Menu 
              -->
              <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">FLOORING<b class="caret-down"></b></a>
                <ul class="dropdown-menu">
                  <li class="grid">
                    <div class="row">
                      <div class="col-sm-3">

                      </div>
                      <!-- // row  -->
                    </div>
                  </li>
                </ul>
              </li>
              <!--Nesting Menu 
              -->
              <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">HOME DECOR<b class="caret-down"></b></a>
                <ul class="dropdown-menu">
                  <li class="grid">

                  <h4>SHOP BY CATEGORY</h4>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="cat-block">
                                <ul>
                                    <li><a href="#">Curtains & Blinds</a></li>
                                    <li><a href="#">Wallpapers</a></li>
                                    <li><a href="#">Wall Decor</a></li>
                                    <li><a href="#">Mirrors</a></li>
                                    <li><a href="#">Rugs & Carpets</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- // cat block  -->

                        <div class="col-sm-6">
                            <div class="cat-block">
                                <ul>
                                    <li><a href="#">Sculptures & Statues</a></li>
                                    <li><a href="#">Bathroom Decor</a></li>
                                    <li><a href="#">Decorative Art</a></li>
                                    <li><a href="#">Other Decor</a></li>
                                    <li class="view-all--btn"><a href="#">View All ></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- // cat block  -->                        
                        
                    </div>
                    <!-- // row  -->

                  </li>
                </ul>
              </li>

              <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Accessories<b class="caret-down"></b></a>
                <ul class="dropdown-menu">
                  <li class="grid">

                  <h4>SHOP BY CATEGORY</h4>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="cat-block">
                                <ul>
                                    <li><a href="#">Curtains & Blinds</a></li>
                                    <li><a href="#">Wallpapers</a></li>
                                    <li><a href="#">Wall Decor</a></li>
                                    <li><a href="#">Mirrors</a></li>
                                    <li><a href="#">Rugs & Carpets</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- // cat block  -->

                        <div class="col-sm-6">
                            <div class="cat-block">
                                <ul>
                                    <li><a href="#">Sculptures & Statues</a></li>
                                    <li><a href="#">Bathroom Decor</a></li>
                                    <li><a href="#">Decorative Art</a></li>
                                    <li><a href="#">Other Decor</a></li>
                                    <li class="view-all--btn"><a href="#">View All ></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- // cat block  -->                        
                        
                    </div>
                    <!-- // row  -->

                  </li>
                </ul>
              </li>        
              
              <li><a href="#">Shop</a></li>
              <li><a href="#">Artisians</a></li>

            </ul>
          </div>
        </div>
      