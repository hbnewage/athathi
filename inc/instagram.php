<section id="instagram-feed">
    <div class="container">
        <header>
            <h4><i class="fab fa-instagram"></i> FOLLOW @INSTAGRAM</h4>
        </header>
    </div>
    <!-- // container  -->
    <div class="instagram-list">
        <div class="item">
            <img src="assets/img/misc/1.jpg" alt="">
        </div>
        <div class="item">
            <img src="assets/img/misc/2.jpg" alt="">
        </div>
        <div class="item">
            <img src="assets/img/misc/3.jpg" alt="">
        </div>
        <div class="item">
            <img src="assets/img/misc/4.jpg" alt="">
        </div>
        <div class="item">
            <img src="assets/img/misc/5.jpg" alt="">
        </div>
        <div class="item">
            <img src="assets/img/misc/6.jpg" alt="">
        </div>																				
    </div>
    <!-- // instagram  -->			
</section>
<!-- // instagram  -->