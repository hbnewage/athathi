<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="assets/css/main.min.css">
</head>
<body>

	<div id="page-top"></div>
    
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="assets/js/custom-dist.js"></script> 

		<div id="top" class="navbar-fixed-top">
			<div id="top-bar">
				<div class="container">
					<div class="top-bar__info hidden-xs">
						<p>CALL US ON: <a href="tel:+973 123 456 7890">+973 123 456 7890</a></p>
					</div>
					<!-- // info  -->
					<div class="top-bar__links">
						<ul>
							<li><a href="#"><i class="fas fa-user"></i>MY ACCOUNT</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">CONTACT US</a></li>
						</ul>
					</div>
					<!-- // links  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // top bar  -->
			<div id="menu-bar">
				<div class="container">
					<div class="branding">
						<a href="index.php"><img src="assets/img/logos/website-logo.svg" alt=""></a>
					</div>
					<!-- // branding  -->
					<div id="main-menu">
						<?php include('inc/nav.php'); ?>
					</div>
					<!-- // main menu  -->
					<div id="actions-bar">
						<ul>
							<li><a href="#"><i class="far fa-search"></i></a></li>
						</ul>
					</div>
					<!-- // actions  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // menu bar  -->
		</div>
        <!-- // top bar  -->
        
        <div id="breadcrumb">
            <div class="container">
                Breadcrumb
            </div>
        </div>
        <!-- // breadcrumb  -->

        <div id="contact-page">
            <div class="container">
                <div class="row">

                    <div class="col-sm-6">
                        <div class="contact-form">
                            <header>
                                <h4>CONTACT FORM</h4>
                            </header>

                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-block">
                                        <label for="">Your Name</label>
                                        <input type="text">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-block">
                                        <label for="">E-Mail Address</label>
                                        <input type="email">
                                    </div>
                                </div>                                

                                
                            </div>
                            <!-- // row  -->

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-block">
                                        <label for="">Phone Number</label>
                                        <input type="tel">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-block">
                                        <label for="">Enquiry</label>
                                        <textarea name="" id=""></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="SUBMIT">
                                </div>
                            </div>

                        </div>
                        <!-- // row  -->
                    </div>
                    <!-- // form  -->

                    <div class="col-sm-6">
                        <div class="info-block">
                            <header>
                                <h5>INFORMATION ABOUT US</h5>
                            </header>
                            <p>Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
                            <div class="info">
                                <h6>CONTACT INFO</h6>
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="info-content">
                                            <img src="assets/img/ico/phon.svg" alt="">
                                            <small><a href="tel:+973 434234242">+973 434234242</a></small>
                                            <small><a href="mailto:athathi@gmail.com">athathi@gmail.com</a></small>
                                        </div>
                                        <!-- // col sm 6  -->
                                    </div>
                                    <!-- // col  -->

                                    <div class="col-md-6">
                                        <div class="info-content">
                                            <img src="assets/img/ico/mob.svg" alt="">
                                            <span>Mobile App</span>
                                            <span>Available for iOS and Android</span>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="info-content">
                                            <img src="assets/img/ico/pin.svg" alt="">
                                            <span>Amwaj Island</span>
                                            <span>Kingdom of Bahrain</span>
                                        </div>
                                    </div>

                                </div>
                                <!-- // row  -->
                                <div class="socials">
                                    <small>Share:</small>
                                    <ul>
                                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- // info  -->
                        </div>
                    </div>
                    <!-- // ifo block  -->

                </div>
                <!-- // row  -->
            </div>
            <!-- // container  -->
        </div>
        <!-- // contact page  -->

		<?php include_once('inc/footer.php'); ?>

</body>
</html>
