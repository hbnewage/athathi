<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="assets/css/main.min.css">
</head>
<body>

	<div id="page-top"></div>
    
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="assets/js/custom-dist.js"></script> 

		<div id="top" class="navbar-fixed-top">
			<div id="top-bar">
				<div class="container">
					<div class="top-bar__info hidden-xs">
						<p>CALL US ON: <a href="tel:+973 123 456 7890">+973 123 456 7890</a></p>
					</div>
					<!-- // info  -->
					<div class="top-bar__links">
						<ul>
							<li><a href="#"><i class="fas fa-user"></i>MY ACCOUNT</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">CONTACT US</a></li>
						</ul>
					</div>
					<!-- // links  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // top bar  -->
			<div id="menu-bar">
				<div class="container">
					<div class="branding">
						<a href="index.php"><img src="assets/img/logos/website-logo.svg" alt=""></a>
					</div>
					<!-- // branding  -->
					<div id="main-menu">
						<?php include('inc/nav.php'); ?>
					</div>
					<!-- // main menu  -->
					<div id="actions-bar">
						<ul>
							<li><a href="#"><i class="far fa-search"></i></a></li>
						</ul>
					</div>
					<!-- // actions  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // menu bar  -->
		</div>
        <!-- // top bar  -->
        
        <div id="breadcrumb">
            <div class="container">
                Breadcrumb
            </div>
        </div>
        <!-- // breadcrumb  -->

        <div id="artisian-content">
            <div class="container">
                <div class="row">

                    <div class="col-lg-5 col-md-6">
                        <div class="image-holder">

                            <header class="visible-xs">
                                <h1>Summit Interior Design</h1>
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <small>(34 Reviews)</small>
                                </div>
                                <!-- // review stars  -->
                            </header>

                            <div id="images-slider">
                                <div class="item">
                                    <img src="assets/img/bg/art-details.jpg" alt="" class="img-responsive">
                                </div>
                                <!-- // sldier item  -->
                                <div class="item">
                                    <img src="assets/img/bg/art-details.jpg" alt="" class="img-responsive">
                                </div>
                                <!-- // sldier item  -->
                                <div class="item">
                                    <img src="assets/img/bg/art-details.jpg" alt="" class="img-responsive">
                                </div>
                                <!-- // sldier item  -->                                                                
                            </div>
                            <!-- // images slider  -->
                        </div>
                    </div>
                    <!-- // image holder  -->

                    <div class="col-lg-6 col-md-6">
                        <div class="content">
                            <header class="hidden-xs">
                                <h1>Summit Interior Design</h1>
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <small>(34 Reviews)</small>
                                </div>
                                <!-- // review stars  -->
                            </header>
                            <div class="description">
                                <p>Summit Interior Design WLL (SID) is a registered Interior Design company in The Kingdom of Bahrain providing architectural interior design consultancy, construction documentation services and procurement.</p>
                                <p>Our goal at SID is to create great interior spaces that skilfully respond to the project brief, that are elegantly detailed, and visibly illustrate the values of the clients personality & branding.</p>
                            </div>
                            <!-- // description  -->
                            <div class="additional-info">

                                <div class="add-block">
                                    <div class="icon-holder">
                                        <a href="tel:"><i class="fal fa-phone"></i></a>
                                    </div>
                                    <!-- // icon  -->
                                    <small><a href="tel:CALL">CALL</a></small>
                                </div>
                                <!-- //add bloc k -->

                                <div class="add-block">
                                    <div class="icon-holder">
                                        <a href="#" target="_blank"><i class="fal fa-globe"></i></a>
                                    </div>
                                    <!-- // icon  -->
                                    <small><a href="#" target="_blank">WEBSITE</a></small>
                                </div>
                                <!-- //add bloc k -->

                                <div class="add-block">
                                    <div class="icon-holder">
                                        <a href="#" target="_blank"><i class="fal fa-map-marker-alt"></i></a>
                                    </div>
                                    <!-- // icon  -->
                                    <small><a href="#">LOCATION</a></small>
                                </div>
                                <!-- //add bloc k -->

                                <div class="add-block">
                                    <div class="icon-holder">
                                        <a href="#"><i class="fal fa-share-alt-square"></i></a>
                                    </div>
                                    <!-- // icon  -->
                                    <small>SHARE</small>
                                </div>
                                <!-- //add bloc k -->                                                                                                

                            </div>
                            <!-- // additional info  -->
                        </div>
                    </div>
                    <!-- // content  -->

                </div>
            </div>
        </div>
        <!-- // artisian content  -->

		<section class="product-list">
			<div class="container">
				<header>
					<h3>Latest</h3>
					<a href="#" class="btn-more">VIEW ALL</a>
				</header>

				<div class="products-slider row">

					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->

					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Fuchsia Tufted Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // item  -->			
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->

					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Fuchsia Tufted Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product1.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // col-md-3  -->
					
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-block">
							<div class="image-holder">
								<img src="assets/img/misc/product2.jpg" alt="">
								<div class="action-block">
									<ul>
										<li><a href="#"><i class="fal fa-heart"></i></a></li>
										<li><a href="#"><i class="fal fa-sync"></i></a></li>
										<li><a href="#"><i class="fal fa-search-plus"></i></a></li>
									</ul>
								</div>
								<!-- // action bloc k -->
							</div>
							<!-- // image  -->
							<div class="content">
								<h6><a href="#">Alaa Aladeen Handicrafts</a></h6>
								<h5><a href="#">Ahmed Eggplant / Gold Sofa</a></h5>
							</div>
							<!-- //  content  -->
							<footer>
								<span class="price">BD 4900</span>
							</footer>
						</div>
						<!-- // blpock  -->
					</div>
					<!-- // item  -->									
					

				</div>
				<!-- // products  -->

			</div>
			<!-- // container  -->
		</section>
		<!-- // product sale  -->       

        <section id="reviews-page">
            <div class="container">
                <header>
                    <h5>Reviews</h5>
                </header>
            </div>
        </section> 
        <!-- // reviews  -->

		<?php include_once('inc/footer.php'); ?>

</body>
</html>
