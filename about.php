<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="assets/css/main.min.css">
</head>
<body>

	<div id="page-top"></div>
    
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="assets/js/custom-dist.js"></script> 

		<div id="top" class="navbar-fixed-top">
			<div id="top-bar">
				<div class="container">
					<div class="top-bar__info hidden-xs">
						<p>CALL US ON: <a href="tel:+973 123 456 7890">+973 123 456 7890</a></p>
					</div>
					<!-- // info  -->
					<div class="top-bar__links">
						<ul>
							<li><a href="#"><i class="fas fa-user"></i>MY ACCOUNT</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">CONTACT US</a></li>
						</ul>
					</div>
					<!-- // links  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // top bar  -->
			<div id="menu-bar">
				<div class="container">
					<div class="branding">
						<a href="index.php"><img src="assets/img/logos/website-logo.svg" alt=""></a>
					</div>
					<!-- // branding  -->
					<div id="main-menu">
						<?php include('inc/nav.php'); ?>
					</div>
					<!-- // main menu  -->
					<div id="actions-bar">
						<ul>
							<li><a href="#"><i class="far fa-search"></i></a></li>
						</ul>
					</div>
					<!-- // actions  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // menu bar  -->
		</div>
        <!-- // top bar  -->

        <div id="breadcrumb">
            <div class="container">
                Breadcrumb
            </div>
        </div>
        <!-- // breadcrumb  -->        
        
        <section id="page-intro">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                        <header>
                            <h1>Welcome to Athathi</h1>
                        </header>
                        <div class="content">
                            <p>Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
                            <a href="#" class="btn-more">Shop Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // intro  -->

        <section id="about-page">
            <div class="container">

                <div class="about-content">
                    <div class="image-holder">
                        <img src="assets/img/bg/about.jpg" alt="" class="img-resonsive">
                    </div>
                    <!-- // image  -->
                    <div class="content">
                        <h4>About Us</h4>
                        <p>Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
                        <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)</p>
                    </div>
                </div>
                <!-- // about content  -->

                <div class="about-content">
                    <div class="image-holder">
                        <img src="assets/img/bg/about2.jpg" alt="" class="img-resonsive">
                    </div>
                    <!-- // image  -->
                    <div class="content">
                        <h4>About Us</h4>
                        <p>Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
                        <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)</p>
                    </div>
                </div>
                <!-- // about content  -->                

            </div>
            <!-- // container  -->
        </section>
        <!-- // about page  -->

		<?php include_once('inc/footer.php'); ?>

</body>
</html>
